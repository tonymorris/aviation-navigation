{-# OPTIONS_GHC -Wall #-}

{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE CPP #-}

module Main(
  main
) where

import Data.Aviation.Navigation ( run )
import Prelude

main ::
  IO ()
main =
  run VERSION_aviation_navigation
